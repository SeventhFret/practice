import "./Navbar.css"
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import { userDataStore } from '../../stores/userData';
import { useEffect } from "react";

export default function Navbar() {
	const loggedIn = userDataStore(state => state.loggedIn)
  const getEmployeeData = userDataStore(state => state.getEmployeeData)
  const employeeData = userDataStore(state => state.employeeData)


  useEffect(() => {
    getEmployeeData()
  }, [getEmployeeData])

	return (
		<Box sx={{ flexGrow: 1 }}>
      <AppBar
        sx={{ backgroundColor: "black", color: "white" }}
        position="fixed"
      >
        <Toolbar>
            <h2 style={{ flexGrow: 1 }}>Unison</h2>

            { loggedIn ? (
              <>
              <h4 style={{ paddingRight: '10px' }}>ID працівника: {employeeData.employee_id}</h4>
              <div className='nav-links-box'>
                <Link to="/dashboard/" style={{ textDecoration: "none" }}>
                  <Button variant="contained">Огляд</Button>
                </Link>
                <Link to="/profile/" style={{ textDecoration: 'none' }}>
                  <Button variant="contained">Профіль</Button>
                </Link>
              </div>
              </>
              
            ) : (
              <div className='nav-links-box'>
                <Link to="/registration/" style={{ textDecoration: "none" }}>
                  <Button variant="text">Реєстрація</Button>
                </Link>
                <Link
                  to="/login/"
                  style={{ textDecoration: "none", marginLeft: "1vw" }}
                >
                  <Button variant="text">Логін</Button>
                </Link>
              </div>
            )}
        </Toolbar>
      </AppBar>
    </Box>
	)
}