// import { useState, useEffect } from 'react'
import { apiClient } from './client'


export const createUser = async (formData) => {
	return await apiClient.post("/users/create/", formData)
}

export const logIn = async (formData) => {
	return await apiClient.post("/users/token/obtain/", formData)
}

export const getOrganizations = async () => {
	return await apiClient.get("/organization/", {
		headers: {
			Authorization: `JWT ${localStorage.getItem('access')}`
		}
	})
}


