import "./Dashboard.css";
import { useState, useEffect } from "react";
import AddIcon from '@mui/icons-material/Add';
import { userDataStore } from "../../stores/userData";
import Navbar from "../../components/Navbar/Navbar";
import { Toolbar } from "@mui/material";

export default function DashboardPage() {
  const getOrganizations = userDataStore((state) => state.getOrganizations);
  const noOrganizations = userDataStore((state) => state.noOrganizations);

  useEffect(() => {
    getOrganizations();
  }, [getOrganizations]);

  return (
    <div>
      <Navbar />
      <Toolbar />

      <div className="content-outer-wrapper">
        <div className="content-wrapper">
          <h1>Огляд</h1>

          {noOrganizations ? (
						<a href="/organization/create/" className="create-organization-link">
							<div className="create-organization-box">
								<AddIcon />
								<h3>Створити організацію</h3>
							</div>
						</a>
          ) : null}
        </div>
      </div>
    </div>
  );
}
