import "./Home.css"
import Navbar from "../../components/Navbar/Navbar"
import Toolbar from '@mui/material/Toolbar';


export default function HomePage() {
	return (
		<div className="main-screen">
			<Navbar />
			<Toolbar />
			
			<section className="first-section">
				<div className="header-block">
					<h1>Unison</h1>
					<h2>Універсальний застосунок керування працівниками.</h2>
				</div>
			</section>
		</div>
	)
}