import "./Login.css";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { logIn } from "../../api/utils";
import Navbar from "../../components/Navbar/Navbar";
import Toolbar from "@mui/material/Toolbar";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Alert } from "@mui/material";
import { userDataStore } from "../../stores/userData";

export default function LoginPage() {
  const navigate = useNavigate();
	const setLoggedIn = userDataStore(state => state.setLoggedIn)
  const formObject = {
    username: "",
    password: "",
  };
  const [formData, setFormData] = useState(formObject);
  const [errors, setErrors] = useState(formObject);
  const [requestErrors, setRequestErrors] = useState([]);

  const changeFormData = (field, value) => {
    setFormData({
      ...formData,
      [field]: value,
    });
  };

  const submitLogin = () => {
    setErrors(formObject);
    setRequestErrors([]);
    let errorDetected = false;
    let errorsObj = formObject;

    Object.keys(formData).forEach((value) => {
      console.log(value);
      if (formData[value] === "") {
        errorDetected = true;
        errorsObj = {
          ...errorsObj,
          [value]: "Це поле не має бути пустим!",
        };
      }
    });

    setErrors(errorsObj);

    if (!errorDetected) {
      logIn(formData)
        .then((res) => {
					console.log(res.data);
					localStorage.setItem("access", res.data.access)
					localStorage.setItem("refresh", res.data.refresh)
					setLoggedIn(true)
					setTimeout(() => {
					  navigate("/dashboard/");
					}, 1000);
        })
        .catch((err) => {console.log(err); setRequestErrors(err.response.data.errors)});
    }
  };

  return (
    <div>
      <Navbar />
      <Toolbar />

      <section className="login-section">
        <div className="form-data-block">
          <h2>Увійти в акаунт</h2>

          <TextField
            error={errors.username !== "" ? true : false}
            helperText={errors.username !== "" ? errors.username : ""}
            value={formData.username}
            label="Email"
            variant="outlined"
            onChange={(event) => {
              changeFormData("username", event.target.value);
            }}
          />

          <TextField
            error={errors.password !== "" ? true : false}
            helperText={errors.password !== "" ? errors.password : ""}
            value={formData.password}
            label="Пароль"
            variant="outlined"
            fullWidth
            type="password"
            onChange={(event) => {
              changeFormData("password", event.target.value);
            }}
          />

          {requestErrors?.length > 0
            ? requestErrors.map((error) => (
                <Alert severity="error">{error}</Alert>
              ))
            : null}

          <Button
            variant="contained"
            size="large"
            onClick={() => {
              submitLogin();
            }}
          >
            Увійти
          </Button>
        </div>
      </section>
    </div>
  );
}
