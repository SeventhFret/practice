import "./Profile.css"
import { useState } from "react";
import { Toolbar, Typography } from "@mui/material";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Divider from "@mui/material/Divider";
import Navbar from "../../components/Navbar/Navbar";
import { userDataStore } from "../../stores/userData";

export default function ProfilePage() {
	const employeeData = userDataStore(state => state.employeeData)
	const [birthDate, setBirthDate] = useState(null)

	return (
		<div>
			<Navbar />
			<Toolbar />

			<div className="content-outer-wrapper">
				<div className="content-wrapper">
					<h1>Профіль працівника</h1>
					<Divider sx={{ marginBottom: '20px' }} />

					<Typography variant="subtitle1">ID працівника: {employeeData.employee_id}</Typography>

					<div className="content-group">
						<div className="full-name-group">
							<div className="full-name-sub-group">
								<Typography variant="h5">Прізвище:</Typography>
								<Typography variant="h6" sx={{ fontStyle: 'italic' }}>{employeeData.last_name}</Typography>
							</div>

							<div className="full-name-sub-group">
								<Typography variant="h5">Імʼя:</Typography>
								<Typography variant="h6" sx={{ fontStyle: 'italic' }}>{employeeData.first_name}</Typography>
							</div>

							<div className="full-name-sub-group">
								<Typography variant="h5">По-батькові:</Typography>
								<Typography variant="h6" sx={{ fontStyle: 'italic' }}>{employeeData.patronymic}</Typography>
							</div>
						</div>

						<div>
							<Typography variant="h5">Заробітна платня</Typography>
							<Typography variant="h6" sx={{ fontStyle: 'italic' }}>{employeeData.salary}</Typography>
						</div>
						
						<div>
							<Typography variant="h5">Дата народження</Typography>
							<LocalizationProvider dateAdapter={AdapterDayjs}>
								<DatePicker value={birthDate} onChange={(newValue) => setBirthDate(newValue)} />
							</LocalizationProvider>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}