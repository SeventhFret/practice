import "./Registration.css";
import { useState } from "react";
import { createUser } from "../../api/utils";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/Navbar/Navbar";
import Toolbar from "@mui/material/Toolbar";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { Alert } from "@mui/material";

export default function RegistrationPage() {
  const navigate = useNavigate();
  const formObject = {
    email: "",
    last_name: "",
    first_name: "",
    patronymic: "",
    password: "",
  };
  const [formData, setFormData] = useState(formObject);
  const [errors, setErrors] = useState(formObject);
  const [requestErrors, setRequestErrors] = useState([]);
  const [registeredSuccessfully, setRegisteredSuccessfully] = useState(false);

  const changeFormData = (field, value) => {
    setFormData({
      ...formData,
      [field]: value,
    });
  };

  const submitRegistration = () => {
    setErrors(formObject);
    setRequestErrors([]);
    let errorDetected = false;
    let errorsObj = formObject;

    Object.keys(formData).forEach((value) => {
      console.log(value);
      if (formData[value] === "") {
        errorDetected = true;
        errorsObj = {
          ...errorsObj,
          [value]: "Це поле не має бути пустим!",
        };
      }
    });

    setErrors(errorsObj);

    if (!errorDetected) {
      createUser(formData)
        .then((res) => {
          if (res.data.success) {
            setRegisteredSuccessfully(true);
            setTimeout(() => {
              navigate("/dashboard/");
            }, 1000);
          }
        })
        .catch((err) => setRequestErrors(err.response.data.errors));
    }
  };

  // console.log(errors);

  return (
    <div>
      <Navbar />
      <Toolbar />

      <section className="registration-section">
        <div className="form-data-block">
          <h2>Реєстрація</h2>

          <TextField
            error={errors.email !== "" ? true : false}
            helperText={errors.email !== "" ? errors.email : ""}
            value={formData.email}
            label="Email"
            variant="outlined"
            onChange={(event) => {
              changeFormData("email", event.target.value);
            }}
          />

          <TextField
            error={errors.last_name !== "" ? true : false}
            helperText={errors.last_name !== "" ? errors.last_name : ""}
            value={formData.last_name}
            label="Прізвище"
            variant="outlined"
            fullWidth
            onChange={(event) => {
              changeFormData("last_name", event.target.value);
            }}
          />

          <TextField
            error={errors.first_name !== "" ? true : false}
            helperText={errors.first_name !== "" ? errors.first_name : ""}
            value={formData.first_name}
            label="Імʼя"
            variant="outlined"
            onChange={(event) => {
              changeFormData("first_name", event.target.value);
            }}
          />

          <TextField
            error={errors.patronymic !== "" ? true : false}
            helperText={errors.patronymic !== "" ? errors.patronymic : ""}
            value={formData.patronymic}
            label="По-батькові"
            variant="outlined"
            onChange={(event) => {
              changeFormData("patronymic", event.target.value);
            }}
          />

          <TextField
            error={errors.password !== "" ? true : false}
            helperText={errors.password !== "" ? errors.password : ""}
            value={formData.password}
            label="Пароль"
            variant="outlined"
            fullWidth
            type="password"
            onChange={(event) => {
              changeFormData("password", event.target.value);
            }}
          />

          {requestErrors?.length > 0
            ? requestErrors.map((error) => (
                <Alert severity="error">{error}</Alert>
              ))
            : null}

          <Button
            color={registeredSuccessfully ? "success" : "primary"}
            variant="contained"
            size="large"
            onClick={() => {
              submitRegistration();
            }}
          >
            { registeredSuccessfully ? "Зареєстровано" : "Зареєструватись" }
          </Button>
        </div>
      </section>
    </div>
  );
}
