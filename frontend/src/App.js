import { Routes, Route } from 'react-router-dom'
import HomePage from './pages/Home/Home';
import RegistrationPage from './pages/Registration/Registration';
import DashboardPage from './pages/Dashboard/Dashboard';
import LoginPage from './pages/Login/Login';
import ProfilePage from './pages/Profile/Profile';

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/registration/" element={<RegistrationPage />} />
      <Route path="/login/" element={<LoginPage />} />
      <Route path="/dashboard/" element={<DashboardPage />} />
      <Route path="/profile/" element={<ProfilePage />} />
    </Routes>
  );
}

export default App;
