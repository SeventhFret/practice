import { create } from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";
import { apiClient } from "../api/client";
import { getOrganizations } from "../api/utils";

export const userDataStore = create(
  persist(
    (set, _) => ({
      loggedIn: false,
      employeeData: {},
      organizations: [],
      noOrganizations: false,

      getOrganizations: () => {
        const state = userDataStore.getState()

        if (state.loggedIn) {
          if (state.organizations.length === 0 && !state.noOrganizations) {
            getOrganizations()
            .then(res => {console.log(res.data); set({ organizations: res.data })})
            .catch(err => {
              if (err.response.status === 404) {
                set({ noOrganizations: true })
              } else {
                console.log(err);
              }
            })
          }
        }
      },
      getEmployeeData: () => {
        const state = userDataStore.getState();

        if (state.loggedIn) {
          if (Object.keys(state.employeeData).length === 0) {
            apiClient
              .get("/users/get/", {
                headers: {
                  Authorization: `JWT ${localStorage.getItem("access")}`,
                },
              })
              .then((res) => {
                set({ employeeData: res.data.profile });
              })
              .catch((err) => console.log(err));
          }
        }

      },
      setLoggedIn: (status) => {
        set({ loggedIn: status });
      },
    }),
    {
      name: "userDataStorage",
      storage: createJSONStorage(() => sessionStorage),
    }
  )
);
