from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from employees.serializers import EmployeeSerializer
from employees.models import Employee
from .serializers import UserSerializer


def flatten_errors(errors: dict):
    all_errors = []

    for field in errors.keys():
        all_errors.extend(errors[field])

    return all_errors

class CreateUserView(APIView):
    permission_classes = [AllowAny]
    
    def post(self, request):
        if User.objects.filter(email=request.data['email']).exists():
            return Response({"errors": ["This email is already taken"]}, status.HTTP_400_BAD_REQUEST)

        user_data = request.data
        user_data['username'] = user_data['email']
        
        user_ser = UserSerializer(data=user_data)
        
        if user_ser.is_valid():
            user = user_ser.save()
            user.is_active = True
            user.save()
            
            employee_ser = EmployeeSerializer(data={"user": user.id, "patronymic": user_data['patronymic']})
            
            if employee_ser.is_valid():
                employee_ser.save()
                
                return Response({"success": True}, status.HTTP_201_CREATED)
        
        return Response({"errors": flatten_errors(user_ser.errors)}, status.HTTP_400_BAD_REQUEST)

class GetEmployeeView(APIView):
    def get(self, request):
        user = request.user
        
        employee = Employee.objects.get(user=user.pk)
        employee_data = {'profile': {
            "employee_id": employee.id,
            "patronymic": employee.patronymic,
            "salary": employee.salary,
            "birthday": employee.birthday,
            "email": user.email,
            "first_name": user.first_name,
            "last_name": user.last_name,
        }}
        
        return Response(employee_data, status.HTTP_200_OK)