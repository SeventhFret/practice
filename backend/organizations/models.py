from django.db import models
from employees.models import Employee

class Organization(models.Model):
    owner = models.ForeignKey(Employee, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=255)
    description = models.TextField()
    