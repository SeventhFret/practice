from django.urls import path
from .views import OrganizationsCrud, GetOrganizationsView


urlpatterns = [
	path("", GetOrganizationsView.as_view()),
	path("<int:pk>/", OrganizationsCrud.as_view())
]
