from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from .models import Organization
from .serializers import OrganizationSerializer


class GetOrganizationsView(APIView):
    def get(self, request):
        user = request.user
        organizations = Organization.objects.filter(owner=user.pk)
        
        serializer = OrganizationSerializer(data=organizations, many=True)
        
        if len(organizations) == 0:
            return Response({"message": "Not found"}, status.HTTP_404_NOT_FOUND)
        
        return Response(serializer.data, status.HTTP_200_OK)
    
    
class OrganizationsCrud(RetrieveUpdateDestroyAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    authentication_classes = [JWTAuthentication]
