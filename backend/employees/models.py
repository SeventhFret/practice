from django.db import models
from django.contrib.auth.models import User

class Employee(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    patronymic = models.CharField(max_length=255, null=True)
    salary = models.IntegerField(null=True)
    birthday = models.DateField(null=True)
    
    def __str__(self):
        return f"{self.user.last_name} {self.user.first_name} {self.patronymic}"
    
