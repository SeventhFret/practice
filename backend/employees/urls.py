from django.urls import path
from .views import UpdateEmployeeView

urlpatterns = [
	path("update/<int:pk>", UpdateEmployeeView.as_view())
]
